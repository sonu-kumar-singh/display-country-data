import React, { Component } from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Content from './Components/Content';
import EachCountryDetail from './Components/EachCountryDetail';
import Header from './Components/Header';
export default class App extends Component {
  constructor(props) {
    super(props)
  
    this.state = {}
  }
  render() {
    return (
      <>
      <Header />
      <Router>
      <Switch>
        <Route exact path="/" component={ Content } />
        <Route path="/:code" component={ EachCountryDetail } /> 
      </Switch>
      </Router>
      </>
    )
  }
}

import React, { Component } from 'react'
import * as CountryApi from './Api'
import './EachCountryDetail.css'
import { Link } from 'react-router-dom'

export default class EachCountryDetail extends Component {
   constructor(props) {
     super(props)
   
     this.state = {
        country: null,
        isLoading: true,
        hasError: false
     }
   }
   async componentDidMount(){
    const {code} = this.props.match.params;
    try{
        const country = await CountryApi.getCountryByCode(code)
        this.setState({
            country: country,
            isLoading: false,
            hasError: false
        })
   
       //console.log(country.data[0].name.nativeName[code.toLowerCase()].official, code.toLowerCase())
    }
   
    catch(err){
        this.setState({
            country: null,
            isLoading: false,
            hasError: `Error occured fetching the data by ${code}`
        })
    }
   
   }

   async componentDidUpdate(prevProps){
    if(prevProps.match.params.code !== this.props.match.params.code){
      const {code} = this.props.match.params;
      try{
        const country = await CountryApi.getCountryByCode(code)
        this.setState({
            country: country,
            isLoading: false,
            hasError: false
        })
    }
   
    catch(err){
        this.setState({
            country: null,
            isLoading: false,
            hasError: `Error occured fetching the data by ${code}`
        })
    }
    }
   }
  render() {
    if(this.state.hasError){
        return (
          <div>
            <h1>{this.state.hasError}</h1>
          </div>
        )
      }
  
  
      if(this.state.isLoading){
        return( <div>
                   <h1>Loading...</h1>
              </div>
      )}
    

        return (
            <>
            <Link to={'/'}><button className='backButton'>Back</button></Link>

            <div className='eachCountryContainer'>
              
             <div className='imgContainer'>
                <img src={this.state.country.data[0].flags.png} alt='country-pic' className='eachCountryPhoto'/>
             </div>
             <div className='countryDataWithBorderContainer'>
                <div className='eachCountryDataContainer'>
                      <div className='listedCountryData'>

                           <div className='countryName'>{this.state.country.data[0].name.common}</div>
                           <ul>
                             {this.state.country.data[0].name.nativeName[this.props.match.params.code.toLowerCase()]
                             && <li><label>Native Name:{this.state.country.data[0].name.nativeName[this.props.match.params.code.toLowerCase()].official}</label></li>}
                              <li><label>Population:{this.state.country.data[0].population}</label></li>
                              <li><label>Region:{this.state.country.data[0].region}</label></li>
                              <li><label>Sub region:{this.state.country.data[0].subregion}</label></li>
                              <li><label>Capital:{this.state.country.data[0].capital[0]}</label></li>
                           </ul>
                      </div>

                      <div className='listedCountryData'>
                          <ul className='dummyClass'>
                          <li><label>Top Level Domain:{this.state.country.data[0].tld[0]}</label></li>
                          <li><label>Currencies:{  this.state.country.data[0].currencies[Object.keys(this.state.country.data[0].currencies)[0]].name}</label></li>
                          <li><label>Languages:{Object.values(this.state.country.data[0].languages).join(', ')}</label></li>
                          </ul>
                      </div>
                </div>

                <div className='borderContainer'>
                    <label>Borders countries:</label>
                  {(this.state.country.data[0].borders || []).map(border => <Link to={border}><button className='borderCountry'>{border}</button></Link>)}
                </div>
             </div>

            </div>

            </>
          )


    
  }
}

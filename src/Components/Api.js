import axios from 'axios'

export const getAllCountries = () => {
  return axios
  .get('https://restcountries.com/v3.1/all')
  .then(res => res.data);
};

export const getCountryByCode = (code) => {
  return axios.get(`https://restcountries.com/v3.1/alpha/${code}`)
}
import React, { Component } from 'react'
import * as CountryApi from './Api'
import './Content.css'
import { Link } from 'react-router-dom'
// console.log(CountryApi)
export default class Content extends Component {


    constructor(props) {
      super(props)
    
      this.state = {
        countries: [],
        isLoading: true,
        hasError: false,
        searchText: '',
        region: 'all'
      }
    }



    componentDidMount(){
   CountryApi.getAllCountries().then((countries) => {
    this.setState({
      countries,
      isLoading: false,
      hasError: false
    })
   }).catch(err => {
    this.setState({
      countries: [],
      isLoading: false,
      hasError: 'Failed to load the countries'
    })
   })
    }

    handleInput = (event) => {
      this.setState({
         searchText: event.target.value
      })
    }

    handleDropDown = (event) => {
      this.setState({
        region: event.target.value
      })
    }


  render() {


    if(this.state.hasError){
      return (
        <div>
          <h1>{this.state.hasError}</h1>
        </div>
      )
    }


    if(this.state.isLoading){
      return( <div>
                 <h1>Loading...</h1>
            </div>
    )}


    const searchResults = this.state.searchText === ''
                           ? this.state.countries 
                           : this.state.countries.filter(eachCountryData => {
                             const searchTextLength = this.state.searchText.length
                            return eachCountryData.name.common.slice(0, searchTextLength).toLowerCase() === this.state.searchText.toLowerCase()
                           }) 

     
    const filteredResult = this.state.region === 'all'
                           ? searchResults
                           : searchResults.filter(eachCountryData => {
                            return eachCountryData.region === this.state.region
                           })

    return (
      <>

          



        <div className='searchBarContainer'>
            <input className='searchInput'type='text' placeholder='Search for a Country' value={this.state.searchText} onChange={this.handleInput}/>
            <select className='dropDown'  onChange={this.handleDropDown}>
                  <option value='all'>Filter by Region</option>
                  <option value='Africa'>Africa</option>
                  <option value='Americas'>Americas</option>
                  <option value='Asia'>Asia</option>
                  <option value='Europe'>Europe</option>
                  <option value='Oceania'>Oceania</option>
           </select>
        </div>
        

        <div className='contentPage'>{filteredResult.map(eachCountryData  => {
              return( 
              <Link to={`/${eachCountryData.cca3}`} key={eachCountryData.cca3}>
                <div className='eachCountryData'>
                <img src={eachCountryData.flags.png} alt='country-pic' className='countryPhoto'/>
                        <ul>
                            <li className='countryName'>{eachCountryData.name.common}</li>
                            <li> <label>Population:{eachCountryData.population}</label></li>
                            <li><label>Region:{eachCountryData.region}</label></li>
                            <li><label>Capital:{eachCountryData.capital}</label></li>
                        </ul>
                </div>
              </Link>
              )
            })}
        </div>

      </>
    )
  }
}
